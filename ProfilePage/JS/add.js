function createNewElement() {
    // First create a DIV element.
	var ingInput = document.createElement('div');
	var weightInput = document.createElement('div');

    // Then add the content (a new input box) of the element.
    ingInput.innerHTML = "<input type='text' class='form-control' aria-describedby='emailHelp' placeholder='Ingredient Name'>";
    weightInput.innerHTML = "<input style='width: 4+0%;' type='text' class='form-control' aria-describedby='emailHelp' placeholder='weight'>"

    // Finally put it where it is supposed to appear.
    document.getElementById("newIngre").appendChild(ingInput);
    document.getElementById("newWeig").appendChild(weightInput);
}